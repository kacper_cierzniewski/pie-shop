﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using pieShop.Models;
using pieShop.ViewModels;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pieShop.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPieRepository _pieRepository;

        public HomeController(IPieRepository pieRepository) // dependency injection
        {
            _pieRepository = pieRepository; 
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            var pies = _pieRepository.GetAllPies().OrderBy(p => p.Name);
            var homeViewModel = new HomeViewModel()
            {
                Title = "Welcome to Pie Shop",
                Pies = pies.ToList()
            };
            return View(homeViewModel);
        }

        public IActionResult Details(int id)
        {
            var pie = _pieRepository.GetById(id);
            if (pie == null)
            {
                return NotFound();
            }

            return View(pie);
        }
    }
}
